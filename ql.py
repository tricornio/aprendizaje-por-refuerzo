# -*- coding: utf-8 -*-

import random
import numpy as np
import matplotlib.pyplot as plt


# Environment size tamano del entorno
width = 5
height = 16

# Actions nº acciones (arriba abajo izq derecha)
num_actions = 4

#
actions_list = {"UP": 0,
                "RIGHT": 1,
                "DOWN": 2,
                "LEFT": 3
                }

# y - x
actions_vectors = {"UP": (-1, 0),
                   "RIGHT": (0, 1),
                   "DOWN": (1, 0),
                   "LEFT": (0, -1)
                   }

# Discount factor
discount = 0.8

# rellenamos la tabla Q
Q = np.zeros((height * width, num_actions))  # Q matrix
# esta tabla es desconocida y es donde decides poner en la tabla las recomepensas
Rewards = np.zeros(height * width)  # Reward matrix, it is stored in one dimension


# te dice que nº de estado es, osea devuelve s1, s2, s3 etc
def getState(y, x):
    return y * width + x


# si le das el estado te dice su coordenada
def getStateCoord(state):
    return int(state / width), int(state % width)

# si le das un estado te devuelve las opciones uqe puedes hacer (arriba, abajo, izq, derecha)
def getActions(state):
    y, x = getStateCoord(state)
    actions = []
    if x < width - 1:
        actions.append("RIGHT")
    if x > 0:
        actions.append("LEFT")
    if y < height - 1:
        actions.append("DOWN")
    if y > 0:
        actions.append("UP")
    return actions

# accion aleatorio
def getRndAction(state):
    return random.choice(getActions(state))


def getRndState():
    return random.randint(0, height * width - 1)


Rewards[4 * width + 3] = -10000
Rewards[4 * width + 2] = -10000
Rewards[4 * width + 1] = -10000
Rewards[4 * width + 0] = -10000

Rewards[9 * width + 4] = -10000
Rewards[9 * width + 3] = -10000
Rewards[9 * width + 2] = -10000
Rewards[9 * width + 1] = -10000

# estado final
Rewards[3 * width + 3] = 100
final_state = getState(3, 3)

print np.reshape(Rewards, (height, width))

# hace el qlearning
def qlearning(s1, a, s2):
    Q[s1][a] = Rewards[s2] + discount * max(Q[s2])
    return



# APARTADO A
# Episodes
cont = 0
n = 100
for i in xrange(n):
    # cogemos un estado al azar
    state = getRndState()
    # caminamos hasta llegar al estado final
    while state != final_state:
        # accion aleatoria para movernos
        action = getRndAction(state)
        # estas 2 lineas nos mueven
        y = getStateCoord(state)[0] + actions_vectors[action][0]
        x = getStateCoord(state)[1] + actions_vectors[action][1]
        # para hacer s y s prima
        new_state = getState(y, x)
        # hacemos esto del q learning
        qlearning(state, actions_list[action], new_state)
        state = new_state
        cont += 1

#print Q
print ""
print "El promedio de acciones en random es: " + str(cont/n)



# APARTADO B Y C
cont = 0
n = 100
for i in xrange(n):
    state = getRndState()
    while state != final_state:
        maxValue = 0
        #
        allActions = getActions(state) # obtenemos todas las acciones posibles (arriba, abajo, izsquierda, derecha)

        # nos quedamos con el estado con mayor valor
        #comprueba los valores de las acciones
        for p in allActions:
            value = Q[state][actions_list[p]] # comprobamos el valor que tiene el estado al que nos movemos
            if ( value > maxValue and value != 0 ):
                maxValue = value
                direction = p
                y = getStateCoord(state)[0] + actions_vectors[p][0] # coordenadas del valor maximo
                x = getStateCoord(state)[1] + actions_vectors[p][1]
                new_state = getState(y, x)

        # decidimos si explorar o explotar
        if ( maxValue == 0 ):
            action = getRndAction(state)
            while ( Q[state][actions_list[action]] < 0 ): # asi evitamos los estados con valor negativo
                action = getRndAction(state)
            y = getStateCoord(state)[0] + actions_vectors[action][0]
            x = getStateCoord(state)[1] + actions_vectors[action][1]
            new_state = getState(y, x)
            qlearning(state, actions_list[action], new_state)
        else:
            qlearning(state, actions_list[direction], new_state)

        state = new_state
        cont += 1

#print Q
print ""
print "El promedio de acciones en greedy es: " + str(cont/n)

# APARTADO B Y C
cont = 0
n = 100
for i in xrange(n):
    state = getRndState()
    while state != final_state:
        maxValue = 0
        #
        allActions = getActions(state) # obtenemos todas las acciones posibles (arriba, abajo, izsquierda, derecha)

        # nos quedamos con el estado con mayor valor
        #comprueba los valores de las acciones
        for p in allActions:
            value = Q[state][actions_list[p]] # comprobamos el valor que tiene el estado al que nos movemos
            if ( value > maxValue and value != 0 ):
                maxValue = value
                direction = p
                y = getStateCoord(state)[0] + actions_vectors[p][0] # coordenadas del valor maximo
                x = getStateCoord(state)[1] + actions_vectors[p][1]
                new_state = getState(y, x)

        # decidimos si explorar o explotar
        #if ( random.random() < 0.05 or maxValue == 0 ):
        if (random.random() < 0.10 or maxValue == 0):
            action = getRndAction(state)
            while ( Q[state][actions_list[action]] < 0 ): # asi evitamos los estados con valor negativo (Q(s,a))
                action = getRndAction(state)
            y = getStateCoord(state)[0] + actions_vectors[action][0]
            x = getStateCoord(state)[1] + actions_vectors[action][1]
            new_state = getState(y, x)
            qlearning(state, actions_list[action], new_state)
        else:
            qlearning(state, actions_list[direction], new_state)

        state = new_state
        cont += 1

#print Q
print ""
print "El promedio de acciones en egreedy es: " + str(cont/n)





# esto para imprimir la tabls
# Q matrix plot

s = 0
ax = plt.axes()
ax.axis([-1, width + 1, -1, height + 1])

for j in xrange(height):

    plt.plot([0, width], [j, j], 'b')
    for i in xrange(width):
        plt.plot([i, i], [0, height], 'b')

        direction = np.argmax(Q[s])
        if s != final_state:
            if direction == 0:
                ax.arrow(i + 0.5, 0.75 + j, 0, -0.35, head_width=0.08, head_length=0.08, fc='k', ec='k')
            if direction == 1:
                ax.arrow(0.25 + i, j + 0.5, 0.35, 0., head_width=0.08, head_length=0.08, fc='k', ec='k')
            if direction == 2:
                ax.arrow(i + 0.5, 0.25 + j, 0, 0.35, head_width=0.08, head_length=0.08, fc='k', ec='k')
            if direction == 3:
                ax.arrow(0.75 + i, j + 0.5, -0.35, 0., head_width=0.08, head_length=0.08, fc='k', ec='k')
        s += 1

    plt.plot([i+1, i+1], [0, height], 'b')
    plt.plot([0, width], [j+1, j+1], 'b')

plt.show()
